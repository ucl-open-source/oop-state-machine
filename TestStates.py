from statemachine import State
import time

class StateA(State):
    def Enter(self):
        self.counter = 0

    def Execute(self):
        self.counter += 1
        print("State A " + str(self.counter))
        time.sleep(1)

        if self.counter == 4:
            self.stateMachine.ChangeState(StateB())


class StateB(State):
    def Execute(self):
        cmd = input("Press c to continue")
        if cmd == "c":
            self.stateMachine.ChangeState(StateA())
